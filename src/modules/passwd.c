/*
    Copyright (C) 2001-2015 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>

#ifdef HAVE_LIMITS_H
#include <limits.h>
#ifndef LINE_MAX
#ifdef _POSIX2_LINE_MAX
#define LINE_MAX _POSIX2_LINE_MAX
#else
#define LINE_MAX 2048
#endif
#endif
#endif

#ifdef HAVE_GETOPT_H
#include <getopt.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifdef HAVE_GETSPNAM
#include <shadow.h>
#endif

#ifdef HAVE_ERR_H
#include <err.h>
#endif

#ifndef HAVE_STRSEP
#include "../strsep.c"
#endif

#ifndef HAVE_ERR_H
#include "../err.c"
#endif

#ifdef WITH_DMALLOC
#include <dmalloc.h>
#endif

#ifndef ALLPERMS
#define ALLPERMS (S_ISUID|S_ISGID|S_ISVTX|S_IRWXU|S_IRWXG|S_IRWXO)
#endif

/* This is the order of options when the 'P' option is specified. It selects
 * all available options. */
#define PASSWD_OPTION_ORDER	"lpugicedsm"
#define PASSWD_OPTION_STRING	"Plpugcedsmi:"

static int amroot;
static char **strings;
static char options[11];	/* NULL terminated. */
static char *gecos_options;

void add_string(char ***, const char *);
char *stamp(time_t, const char *);
char *safe_strncat(char *, const char *, size_t);

void ui_module_init(int *chainable)
{
    /*
     * Keep the password file open if possible (*BSD). 
     */
#ifdef HAVE_SETPASSENT
    setpassent(1);
#endif

    if (getuid() == 0)
	amroot = 1;

    *chainable = 0;
}

void ui_module_exit()
{
#ifdef HAVE_GETSPNAM
    if (amroot)
	endspent();
#endif

    endpwent();
#ifdef HAVE_GETGRENT
    endgrent();
#endif
}

/* See if the gecos options are valid. */
static int parse_gecos_options(const char *args)
{
    int i = 0;

    for (i = 0; i < strlen(args); i++) {
	switch (args[i]) {
	    case 'n':
	    case '1':
	    case '2':
	    case '3':
	    case 'a':
		break;
	    default:
		return 1;
	}
    }

    return 0;
}

/* Break up the gecos string into sections and add the sections to the output
 * string array if needed. */
static void gecos_strings(char *str)
{
    int i = 0;
    char *buf;
    const char *name, *first, *second, *third;

    name = first = second = third = "-";

    while ((buf = strsep(&str, ",")) != NULL) {
	if (!buf[0])
	    continue;

	switch (i++) {
	    case 0:
		name = buf;
		break;
	    case 1:
		first = buf;
		break;
	    case 2:
		second = buf;
		break;
	    case 3:
		third = buf;
		break;
	    default:
		break;
	}
    }

    for (i = 0; i < strlen(gecos_options); i++) {
	switch (gecos_options[i]) {
	    case 'n':
		add_string(&strings, name);
		break;
	    case '1':
		add_string(&strings, first);
		break;
	    case '2':
		add_string(&strings, second);
		break;
	    case '3':
		add_string(&strings, third);
		break;
	    case 'a':
		add_string(&strings, name);
		add_string(&strings, first);
		add_string(&strings, second);
		add_string(&strings, third);
		break;
	    default:
		break;
	}
    }
}

/* Get all groups that a user is a member of. The primary group will be the
 * first added. */
static void groups(const struct passwd *pw, const int multi,
		   const int verbose)
{
    struct group *grp;
    char tmp[255];
    char line[LINE_MAX];
    gid_t primary = -1;

    line[0] = '\0';

    if ((grp = getgrgid(pw->pw_gid)) == NULL) {
	snprintf(tmp, sizeof(tmp), "%li%s%s%s", (long) pw->pw_gid,
		 (verbose) ? "(" : "", (verbose) ? "!" : "",
		 (verbose) ? ")" : "");
	add_string(&strings, tmp);
	return;
    }

    primary = grp->gr_gid;
    snprintf(tmp, sizeof(tmp), "%li%s%s%s%c", (long) pw->pw_gid,
	     (verbose) ? "(" : "", (verbose) ? grp->gr_name : "",
	     (verbose) ? ")" : "", multi);
    safe_strncat(line, tmp, sizeof(line));
#ifdef HAVE_GETGRENT
#ifdef HAVE_SETGROUPENT
    setgroupent(1);
#else
    setgrent();
#endif

    while ((grp = getgrent()) != NULL) {
	char **members = grp->gr_mem;

	while (*members) {
	    if (strcmp(*members++, pw->pw_name) == 0) {
		if (grp->gr_gid == primary)
		    continue;

		snprintf(tmp, sizeof(tmp), "%li%s%s%s%c", (long) grp->gr_gid,
			 (verbose) ? "(" : "", (verbose) ? grp->gr_name : "",
			 (verbose) ? ")" : "", multi);
		safe_strncat(line, tmp, sizeof(line));
	    }
	}
    }
#endif

    /*
     * Trim the remaining multi-string deliminator.
     */
    line[strlen(line) - 1] = '\0';

    add_string(&strings, line);
}

/* This is output if the -h command line option is passed to the main program.
 */
void ui_module_help()
{
    printf("  Password/Group file information [-P (-%s)]:\n",
	   PASSWD_OPTION_ORDER);
    printf("\t-l  login name\t\t");
    printf("\t-p  encrypted password\n");
    printf("\t-u  user id (uid)\t");
    printf("\t-g  group id (gid)\n");
    printf("\t-c  password change time");
    printf("\t-e  password expire time\n");
    printf("\t-d  home directory\t");
    printf("\t-m  home directory mode\n");
    printf("\t-s  login shell\n");
    printf("\t-i  gecos (any of [n]ame,[1]st,[2]nd,[3]rd or [a]ll)\n\n");
}

/* This is the equivalent to main() only without argc and argv available. */
int ui_module_exec(char ***s, const struct passwd *pw, const int multi_char,
		const int verbose, char *tf)
{
    char *p = options;
    struct stat st;

#ifdef HAVE_GETSPNAM
    struct spwd *spwd = NULL;
#endif

#ifdef HAVE_GETSPNAM
    if (amroot) {
	if ((spwd = getspnam(pw->pw_name)) == NULL)
	    warnx("%s", "getspnam(): unknown error");
    }
#endif

    strings = *s;

    while (*p) {
	char tmp[256];

	switch (*p) {
#ifdef HAVE_GETSPNAM
	    case 'c':
		if (!amroot || !spwd) {
		    add_string(&strings, "!");
		    break;
		}

		snprintf(tmp, sizeof(tmp), "%li", (long) spwd->sp_max);
		add_string(&strings, tmp);
		break;
	    case 'e':
		if (!amroot || !spwd) {
		    add_string(&strings, "!");
		    break;
		}

		snprintf(tmp, sizeof(tmp), "%li", (long) spwd->sp_expire);
		add_string(&strings, tmp);
		break;
#else
	    case 'c':
#ifdef HAVE_PASSWD_CHANGE
		snprintf(tmp, sizeof(tmp), "%li", (long) pw->pw_change);
		add_string(&strings, tmp);
#else
		add_string(&strings, "!");
#endif
		break;
	    case 'e':
#ifdef HAVE_PASSWD_EXPIRE
		snprintf(tmp, sizeof(tmp), "%li", (long) pw->pw_expire);
		add_string(&strings, tmp);
#else
		add_string(&strings, "!");
#endif
		break;
#endif
	    case 'l':
		add_string(&strings, pw->pw_name);
		break;
	    case 'd':
		add_string(&strings, (pw->pw_dir && pw->pw_dir[0]) ? pw->pw_dir : "-");
		break;
	    case 's':
		add_string(&strings, (pw->pw_shell && pw->pw_shell[0]) ? pw->pw_shell : "-");
		break;
	    case 'p':
#ifdef HAVE_GETSPNAM
		if (!amroot)
		    add_string(&strings, (pw->pw_passwd
				&& pw->pw_passwd[0]) ? pw->pw_passwd : "-");
		else if (!spwd)
		    add_string(&strings, "-");
                else
		    add_string(&strings, (spwd->sp_pwdp
				&& spwd->sp_pwdp[0]) ? spwd->sp_pwdp : "-");
#else
		add_string(&strings, (pw->pw_passwd
			    && pw->pw_passwd[0]) ? pw->pw_passwd : "-");
#endif
		break;
	    case 'u':
		sprintf(tmp, "%li", (long) pw->pw_uid);
		add_string(&strings, tmp);
		break;
	    case 'g':
		groups(pw, multi_char, verbose);
		break;
	    case 'm':
		if (!pw->pw_dir || !*pw->pw_dir || stat(pw->pw_dir, &st) == -1) {
		    add_string(&strings, "!");
		    break;
		}

		sprintf(tmp, "%.4o", (unsigned) st.st_mode & ALLPERMS);
		add_string(&strings, tmp);
		break;
	    case 'i':
#ifdef HAVE_PASSWD_GECOS
		gecos_strings(pw->pw_gecos);
#else
		add_string(&strings, "!");
#endif
		break;
	    default:
		break;
	}

	p++;
    }

    *s = strings;
    return EXIT_SUCCESS;
}

const char *ui_module_options_init(char **defaults)
{
    *defaults = (char *)"P";
    return PASSWD_OPTION_STRING;
}

/* Check module option validity. */
int ui_module_options(int argc, char **argv)
{
    int opt;
    char *p = options;

    while ((opt = getopt(argc, argv, PASSWD_OPTION_STRING)) != -1) {
	switch (opt) {
	    case 'i':
		if (parse_gecos_options(optarg))
		    return 1;

		gecos_options = optarg;
		break;
	    case 'P':
		strncpy(options, PASSWD_OPTION_ORDER, sizeof(options));
		gecos_options = (char *)"a";
		return 0;
	    case 'l':
	    case 'p':
	    case 'u':
	    case 'g':
	    case 'c':
	    case 'e':
	    case 'd':
	    case 's':
	    case 'm':
		break;
	    case '?':
		warnx("passwd: invalid option -- %c", optopt);
	    default:
		return 1;
	}

	*p++ = opt;
	*p = '\0';
    }

    return 0;
}
