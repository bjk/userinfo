/*
    Copyright (C) 2001-2015 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef UI_H
#define UI_H

#ifdef HAVE_LIBGEN_H
#include <libgen.h>
#endif

#ifdef HAVE_DLFCN_H
#include <dlfcn.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifdef HAVE_ERR_H
#include <err.h>
#endif

#ifdef HAVE_LIMITS_H
#include <limits.h>
#endif

#ifdef HAVE_PATHS_H
#include <paths.h>
#else
#ifndef _PATH_DEV
#define _PATH_DEV	"/dev/"
#endif
#endif

#define COPYRIGHT "Copyright (C) 2001-2015 " PACKAGE_BUGREPORT
#define STAT(file, sstat) ((followsymlinks) ? stat(file, sstat) : lstat(file, sstat))
#define TIMEBUFSIZE		64
#define UNKNOWN		"!"
#define NONE		"-"
#define ON		"1"
#define OFF		"0"
#define MODULE_DUP		0x001
#define MODULE_OUTPUT		0x002
#define MODULE_CHAINED		0x004
#define MODULE_CHAINABLE	0x008
#define MODULE_VERBOSE		0x010
#define SET_FLAG(var, f)	(var |= f)
#define TEST_FLAG(var, f)	(var & f)

enum {
    OUTPUT_DONE,
    OUTPUT_APPEND
};

/* These are functions found in loadable modules. */
typedef void (module_init) (int *);
typedef void (module_exit) (void);
typedef void (module_help) (void);
typedef char *(module_options_init) (char **);
typedef int (module_options) (int argc, char **argv);
typedef int (module_exec) (char ***, const struct passwd *, const int,
			   const int, char *);

static struct module_s {
    char name[PATH_MAX];	/* The filename of the module. */
    void *m;		/* The module handle. */
    unsigned flags;	/* dup, chained, chainable, and output. */
    char **argv;
    int argc;
} **modules;

static int verbose, delimchar, multichar, usefile;
static int followsymlinks;
static char tf[TIMEBUFSIZE];
static int chaining;
static int chain_output;
static int module_total;

#ifdef HAVE___PROGNAME
extern char *__progname;
#else
static char *__progname;
#endif

#ifdef WITH_DMALLOC
#include <dmalloc.h>
#endif

#endif
